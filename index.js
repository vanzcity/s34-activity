// Initialization
const express = require('express')

const app = express() // Initializing express as the 'app' variable

const port = 3000


// Registration
app.use(express.json()) // express.json() middleware allows our application to read JSON data

app.use(express.urlencoded({extended:true})) // urlencoded middleware allows our application to read data from forms


// [SECTION] Routes
app.get('/home', (request, response) => {
	response.send('Welcome to home page')
})

let users = [{"username": "johndoe",
					"password": "johndoe1234"}]; // Mock database

app.get('/users', (request, response) => {
	
	users = JSON.stringify(users)
	response.send(users)
})



app.delete('/delete-user', (request, response) => {

	
		response.send(`user ${request.body.username} has been deleted.`)
})



// Listening
app.listen(port, () => console.log(`Server is running at localhost:${port}`))
